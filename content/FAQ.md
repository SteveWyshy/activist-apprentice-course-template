# Frequently Asked Questions

### Why do we need more Apps?

Training materials are commonly shared as PDFs or made available as websites. PDFs are great because you can print physical copies, share the digital versions easily enough, and allow Authors to present information as they desire. Websites are nice because they are widely available, easily updated, and provide additional multimedia functionality.

The value of having your course as an app is that it puts the content where Readers are spending a significant amount of their time already, their phones. Additionally, it allows them to get updates to this material the same way they are getting updates to their social media apps. We believe that by putting it side by side with these platforms it'll make it easier to engage with.

Depending on the interest and demand for this project we are considering seeking additional funding to add functionality to support audio / video playback directly in courses, enable quizzes, and investigate how to incorporate a tool like this with messaging services like WhatsApp or Signal.

In addition there are a few more high level reasons you might consider putting your curriculum/training materials into an app.

## How does offline access work?

When paired with [**Station**](https://www.getstation.org), Courses are able to be shared peer to peer among individuals in communities that have low bandwidth access or limited to access to App stores. Station provides the ability to send apps from one device to another device.

## What's the value of version control for training materials?

By utilizing tools originally designed for software Authors are able to revert/review previous versions of their content. This is critical for allowing Authors to track each other's edits and correct mistakes. The long term thinking here is that the more content that is written in this format, the easier it will be for different Authors to mix and share content with significantly less effort.

## Do I have to make updates to a Course App?

Pushing updates to a Course is not required, it was easily incorporated though so we wanted to make it known to Authors. While you may be more familiar with publishing content as a websites App updates are convenient because they require less effort from the Reader to get the content and also removes the hassle of having to track/monitor PDFs files or a website.

---
