# Installing Author Tools

How to install and configure the publisher tools on your local computer.

* NodeJS
* ContentAsCode
* Atom


## NodeJS

There are a number of files which start with a dot in a repo.

One of these is the `.gitignore` file which should specify not to check in the node_modules repo.

Please make sure the `.gitignore` file is the same as in the course template repo (and check for possible other missing dot files).

Then use :

> `git rm -r --cached node_modules`

This will remove the `node_modules` from git, but keep it in your files.

Then `git push`


----
