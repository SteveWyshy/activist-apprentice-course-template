# GitLab account creation

The Activist Apprentice Course Template requires a hosted version of git, either GitLab or GitHub. These docs use GitLab, if you are more familiar with GitHub and are confident you can handle this there, you are obviously free to do so.

---

#### Note: If you are the publisher as well as the author you can use the same GitLab account you created in Chapter 1, section 2, [GitLab account creation](/content/1-publisher-onboard/2-gitlab-account-creation.md).

---

## Account Creation

https://gitlab.com/users/sign_in

Create an account with a reliable email address.

The security of this account is important, so be sure to use a strong password. If you're not using a password manager, [this would be a good time to set one up](https://medium.com/@mshelton/password-managers-for-beginners-d1f49866f80f).

---

### Confirm your account to proceed

Sign into your email account, and you should get an email with the subject: "Confirmation Instructions"

Click the "Confirm your account" link inside of it.

Your browser will open to a GitLab.com sign in page, at the top you should see a confirmation that you have verified your account and now you can sign into your account.

Sign into your account


---

### More info

GitLab is a powerful site, and if you'd like to understand more about what you can do with it [please look into their docs](https://docs.gitlab.com/ce/user/).

---
