# 2. Author Onboarding

Course authors are able to manage their own workflow. There are some technical aspects involved, but this documentation is here to help you understand how to accomplish the different tasks involved.

---

## Course template author

* Install this repo by [following the instructions in the README](https://gitlab.com/contentascode/activist-apprentice/blob/master/README.md#install)
* Once you `apprentice init` the latest version of the course template will be cloned (from https://github.com/smallworldnews/activist-apprentice-course-template.git).
* Edit the content via the workspace (which is a symlink to the locally checked out git repo).
  * `cd WORKSPACE/@apprentice/preview`
  * Open your favorite editor here.

* `apprentice load` can be done manually by:
  * `cd ~/.content/packages/activist-apprentice-course-template`
  * `git pull` to make sure you have the latest changes (in case someone else made a change)

* `apprentice save` can be done manually by:
  * `cd ~/.content/packages/activist-apprentice-course-template`
  * `git pull` to make sure you have the latest changes (in case someone else made a change)
  * `git commit -ma "COMMIT MESSAGE"` (replace COMMIT MESSAGE by a short sentence explaining your changes)
  * `git push` to push your changes to github.

---

## Author starting from the course template

* Do the same thing as for the course template author.
* You can work locally with your favorite editor and change the template as you like.
* In order to save your work on Gitlab you need to create a fork using this link https://gitlab.com/contentascode/activist-apprentice-course-template/forks/new
* Then setup the content package so that it refers to the fork by doing:
  * `git remote remove origin`
  * `git remote add origin URL_OF_YOUR_FORK` that's the URL on the home page of the Gitlab page for the project.

![](images/)

You can now use the exact same manual steps as described for apprentice load and apprentice save!


SAFETAG CLI Getting Started Guide
https://github.com/contentascode/safetag/blob/master/docs/guide.md

Detailed Guide of the Walkthrough
https://www.youtube.com/watch?v=tf4WTG7nvoU
