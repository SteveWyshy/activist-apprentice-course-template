# Generate a Keystore

To be able to generate your app, you need to create a keystore as well as a signing key as well. The keystore, as it sounds is where your key is stored. The key itself is a unique file you create so that if you ever need to update the app, the app stores know the app came from you.

## Create a keystore and key to sign your application.

Here are some good documents to learning about keys.

[about managing your signing keys](https://developer.android.com/studio/publish/app-signing#self-manage) and [securing them](https://developer.android.com/studio/publish/app-signing#secure-key).

We use docker to wrap the [recommended instructions to create a keystore from the command line](https://developer.android.com/studio/publish/app-signing#signing-manually), feel free to change `my-keystore` and `my-key-alias`, as well as other options if needed.

```
cd ~/workspace # Or another secure place to store your keystore
docker run -it -v $(pwd)/keys:/keys iilab/android:24.1.1 bash -c "keytool -genkey -v -keystore /keys/my-keystore.jks -keyalg RSA -keysize 2048 -validity 10000 -alias my-key-alias"
```

---
