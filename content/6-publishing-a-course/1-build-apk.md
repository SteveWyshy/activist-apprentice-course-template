# Building an APK of your Course

We are still actively developing the process for creating an APK that can be uploaded to the Station Repo.

> **The current workflow is documented here, take note though that this may change.**

## Manual APK creation instructions with Docker

Here are instructions to create an unsigned APK with the tooling I have packaged inside a docker image.


First install Docker https://docs.docker.com/docker-for-mac/install/

Then make sure that you set Docker to minimum 8GB memory

> Click **Docker icon**
>Click **Preferences**
>Click **Advanced**
>Set minimum memory to **8GB**

Open up Terminal.

Update the lesson project with the latest version of the mobile packages (this needs to be done with each unique project you may have):

```
cd ~/.content/packages/activist-apprentice-course-template
npm i --save contentascode-mobile@0.2.1
npm i --save metalsmith-contentascode-mobile@0.0.10
```

Commit and push the updated project to gitlab in order for the project to be published to expo:

```
git add .
git commit -m "Updated mobile packages"
git push
```

Generate the mobile project:

```
cd ~/workspace # (change this to where your content as code workspace is located)
apprentice build mobile --project @iilab/safely-securely-producing-media --keystore_path $(pwd)/keys/my-keystore.jks --key_alias my-key-alias mobile # This launches the docker based apk creation
```
You will be prompted to enter your keystore and key alias passwords. If you need to do this, please see the previous lesson.

On first run, the `apprentice build mobile` command will download the docker container which can take a while.

After this you should have an apk called `safely-securing-producing-media.apk` inside the `build` folder in your workspace. You can drag and drop this into RepoMaker.

---
