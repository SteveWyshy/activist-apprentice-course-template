# Publishing a Course

The publishing workflow without server side automation will follow the following steps:

* Install android building requirements (minimum includes SDK, possibly Android Studio will be needed)
* Use `content run android` to preview the app locally
* Edit theming files to change theme colors, font, logo and about page.
* Use `content build android` to generate the APK locally
  - (this would be for local testing. When reproducible builds are setup there would be additional steps for signing keys).
* Use `content publish` to submit the source code to F-Droid.
* RepoMaker will automatically pickup the updated APK when F-Droid has finished building it (the fdroid public build server runs once a day).
*
*
*
Generating the APK locally also requiring installing Android build tools locally and manually uploading to RepoMaker for distribution.


### Information/Images Needed for SWN Repo Listing

App Icon

By (Org/Entity Name)

Short Summary (40 characters)

Description

Screenshots

Featured Graphic

List of Applicable Categories
