# Publish APK

Great job, you've got an APK and you'd like to share it with the world.

If you'd like to have your Course listed in Station, you can submit a request to Small World News via the [Issues board](https://gitlab.com/smallworldnews/station-repo/issues) on the **Station Available Courses** project. If you create an issue there with your GitLab account, it will send some emails to the appropriate people who will follow up about getting the APK listed.

## F-droid

If you'd like to also have your app listed inside of F-Droid, you can [follow the instructions](https://gitlab.com/fdroid/fdroiddata/blob/master/CONTRIBUTING.md#adding-a-new-app) they have provided.

---
