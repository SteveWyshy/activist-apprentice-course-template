# Creating a New Project

Now that we have all the tools in place the next step is setting up a Project on GitLab.

## Create New Project on GitLab in Browser

Go to gitlab.com in your preferred web browser and if necessary, sign into the account you created.

From your home page look for the green button that says "New Project", or click here: https://gitlab.com/projects/new

You'll go to a New project page with some different fields to add text to.

Give your project a name that is brief but specific. If your Course is based on an existing training guide we suggest using the name of it.

When you're ready click the "Create Project" button at the bottom of the page. The page should update and show you that the repository for the project is empty.

Next we are going to clone this project to our local machine with the GitHub Desktop App.

---

## Clone New Project to Local Machine

Open the GitHub Desktop app and go to File > Clone Repository, and a pop up menu should appear with three tabs: GitHub.com | Enterprise | URL

Click on the **URL** tab.

Return to your browser window and in the middle there should be a text box that has the URL of the repository or "repo" for short in the middle of it. There will be a button on the right of it, if you hover over the icon it should display some text **"Copy URL to clipboard"**. Click this to paste the URL.

Return to the GitHub Desktop app and **paste the URL** into the first text field.

Choose the location on your local machine you would like to store your project. Select a location that is easy to navigate to. We suggest setting up a Projects folder in your user folder.

When you are ready click the **"Clone"** button.

If this is the first time you are doing this it will ask you to sign into your GitLab account, if so use your **GitLab credentials** to sign in.

You will now have a window showing no changes have been made to your current repository.

You have now successfully connected your GitLab project to your local machine.

---

### Notes:

*If you are apart of a Group on GitLab be sure to change the project URL from your username to the Groups name before you create it.*

*Long names can sometimes create issues when you are working with them.*

---
