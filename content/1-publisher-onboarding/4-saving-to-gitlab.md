# Commit to Master (GitLab)

Now that we have updated the project files it's time to push those updates onto GitLab so the project for our Course is ready.

## Open GitHub Desktop

Now that we've edited the three files you will see these changes listed. This is how you can track and monitor your own work outside of the documents themselves.

At the bottom left of the window you'll see what looks like a small email window.

---

## Write Summary

In as clear language as possible, summarize the work you've done. Since we've updated files to tie this project to our course specifically we suggest something like "Define project template fields in necessary project files"

If you are collaborating with someone on this project, the summary of a commit becomes the subject of an email they will receive. So be specific but brief.

## Write Description

Here is where you want to be specific in case there is a problem and you need to go back through your work. The clearer you can make your changes the easier it is if you ever need to go back and review your work. Also, if you are collaborating with someone you can use this description field to tell them what you've done.

In our case we can just say something like "Updated template files to tie them to our specific project."

If you were contributing a lot of work or making significant changes, this is where you would like to detail them so your collaborators would know about the work you've done.

---

## Commit to Master

There is a blue button at the bottom left of the window, click this when you are ready.

Your work will be processed, and depending on how much needs to be uploaded it may take a moment or two.

---

## Push Origin

Your work has now been uploaded but now you need to push it to your branch.

In the top bar of the GitHub Desktop app there is a button on that far right that label will change depending on the state of the project you're working on. After you commit your work to your master branch though you will need to do a final Push as well.

Click on **Push origin** in the top bar of the GitHub Desktop app.

It will again process.

Once you see "Fetch origin" in this location your changes have been pushed.

To double check this you can click on the "History" section. You should see your commit listed there.

---
