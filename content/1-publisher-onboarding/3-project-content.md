# Setting Up Project Content Folder

Now that we have our GitLab project setup we will want to get it setup with all the necessary files and folders inside of it so it's a fully functioning Activist Apprentice project.

To briefly state where we're at in the process. We have the activist apprentice project files loaded and also have a new GitLab project that we need to copy that content into.

To do this we're going to open Terminal.

> In case you're returning and you are in a location you don't want to be use the command `cd /` to return to your user directory.

enter the following command to navigate to the activist apprentice content folder:

`cd WORKSPACE/@apprentice/preview`

to open this folder in Finder enter this command:

`open .`

A new finder window will open with the folders and files you downloaded when you ran apprentice init.

Select all these files and go to the Edit Menu and select "Copy".

Now navigate to your unique project folder, if you've followed our instructions it will be somewhere like `/user/projects/[YOUR-COURSE-PROJECT]`

When you've opened this window, go to the Edit Menu and select "Paste".

All the files should be pasted into the directory.

---

Now that we have the generic Activist Apprentice content we need to update it for our own Course.

## Updating Files for our Course

There are a small set of files in the folder where we just copied everything into that we need to update to tie this project to the details specific to our Course. Be sure you are doing this in the folder tied to your GitLab Project and not the preview folder you just copied from.

* content.yml
* package.json
* package-lock.json

---

### content.json

Right click on the `content.json` file and open it with Atom.

There are two changes you will have to update in this file:

* line 10
* line 12

#### Course Name (line 10)

On line 10 you will have up give your Course a name. This is what the Course will be called in the app so make sure it's right.

Replace the "Activist Apprentice Course Template" text with the name of your own Course.

#### Project Slug (line 12)

On line 12 you will have to input the slug of the Project as it's defined on your GitLab Project. The Activist Apprentice GitLab Project's full slug is "activist-apprentice-course-template", remove this and replace it with the slug given to your project.

When you've updated both and double checked them, Go to **File** and Click **Save**.

---

### package.json

Return to your Finder window and right click on the `content.yml` file and open it with Atom.

There are two changes you will have to update in this file:

* line 1
* line 4

> You will likely notice that the the URL on line 8 is tied to the activist-apprentice-course-template project on gitlab. It is necessary for this to remain, do not change this.

#### Course Name (line 1)

On line 1 you will have to input the slug of the Course's project as it's defined on your GitLab Project. The Activist Apprentice GitLab Project's full slug is "activist-apprentice-course-template", remove this and replace it with the slug given to your project.

> **Note:** The quotation marks are necessary, do not remove them or your course will not build.

#### Course Description (line 4)

On line 4 replace the "Activist Apprentice Course Template" text with the name of your own Course. if you'd like to shorten it or edit it slightly you can do so.

> **Note:** Again, the quotation marks are necessary, do not remove them or your course will not build.

When you've updated both and double checked them, Go to **File** and Click **Save**.

---

### package-lock.json

Return to your Finder window and right click on the `package-lock.json` file and open it with Atom.

There is one change you will have to update in this file:

* line 1

#### Course Name (line 1)

On line 1 you will have to input the slug of the Course's project as it's defined on your GitLab Project. The Activist Apprentice GitLab Project's full slug is "activist-apprentice-course-template", remove this and replace it with the slug given to your project.


 When you've updated the file and double checked it, Go to **File** and Click **Save**.

 ---
