# apprentice init

## Open Terminal, Start Node

When you first open Terminal you will need to start up Node. You can do this by running the following command: `nvm use node 8.9.4`

You will get a response saying `Now using node v8.9.4 (npm v5.6.0)`

This means Node is running.

---

## Create Workspace

To confirm you are in the correct location, you can type `pwd` which stands for *Print Working Directory*. This will print out in text where you are at in your computers file system.

If you are in a new Terminal window it will likely print `/Users/[COMPUTER_NAME]` with `[COMPUTER_NAME]` being whatever the name is of your individual computer.

This is a good directory to setup your workspace folder in.

To create your workspace folder, you can enter the following command: `mkdir workspace`

Now we want to switch to this folder. To do so enter the following command: `cd workspace`

You are now inside of the folder you created. If you run `pwd` again you should see this:

`/Users/[COMPUTER_NAME]/workspace`

---

## Run apprentice init

Now that we're in the workspace folder we can run the program necessary to set the folder up for activist apprentice use. This is called initializing or "init" for short.

`apprentice init` is the command you enter in your command line window to initialize a folder with the activist apprentice course template.

>If you're new to using a command line tools, please see our section on Command Line interfaces in the introduction section.


When you press enter after typing `apprentice init` the latest version of the [course template](https://gitlab.com/smallworldnews/activist-apprentice-course-template.git) will be cloned from [gitlab.com/smallworldnews/activist-apprentice-course-template](https://github.com/smallworldnews/activist-apprentice-course-template.git).

This has collected all the files you need on your local machine necessary. Before we begin editing though we need to create the Project on GitLab.

---
