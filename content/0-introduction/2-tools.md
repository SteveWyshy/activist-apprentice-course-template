# Required Command Line Tools

You will need to first install some software tools in order to be able to work on a Course. This will involve downloading some software installation files from the web as well as installing some additional tools with your command line tools.

The Software Tools you'll need & we'll walk you through installing.

* **Node**
  * *v6 minimum, v8.9 LTS recommended*
* **npm**
  * v5.6.0
* **Docker**
  * *v13*

---

> **Technical Note:**
> Specific technical commands will be included throughout this documentation, but will be written for use in **Terminal in OS X**. Please adjust as necessary for your specific CLI.

---

## Node

To install Node download the file labeled **recommended for most users** available on this site here: https://nodejs.org/en/download/

An approximately 15mb file will be downloaded to your computer.

Double click on it and an installation prompt will walk you through an installation process like any other piece of software. We recommend you use the default settings the walkthrough suggests unless you are confident as to why you are making the changes.

Once this is installed you can open a Terminal window and type `node -v` and press enter.

On a new line the version of Node you've installed will display, for our purposes you want to run `v8.9.4`

Now you have verified that the necessary version of Node is installed.

---

## npm v5.6.0

Good news, npm gets installed when you install Node. But before we continue we just want to verify it's there.

Go to your Terminal window and type `npm -v`

What we want to do is change that to version 5.6.0

To change that you need to run this command `npm install -g npm@5.6.0`

This time you want to make sure it is at least version `5.6.0`

Now you have verified that you have the necessary version of npm installed. If you'd like to learn more about Node and npm you can go to the official site for it. https://www.npmjs.com/get-npm

---

## Docker

Follow the appropriate link to download the version of Docker for your computer:

* Mac:
  * https://docs.docker.com/docker-for-mac/install/
* Windows:
  * https://docs.docker.com/docker-for-windows/install/
* Linux:
  * https://docs.docker.com/install/linux/docker-ce/ubuntu/

After you've installed it, open it and go to Docker > **Preferences**

Click on the Gear icon that is labeled **Advanced**

There should be a slider labeled **Memory**, move the slider to the right until it says **8 GB**

Click on the General section, and it will give you a pop up asking to Apply the changes. Click **Apply**

You can close the menu, and Docker should only be needed when running `apprentice build` not `apprentice start`.

---
