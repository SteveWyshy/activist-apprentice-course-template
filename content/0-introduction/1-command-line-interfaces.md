# Command Line Interfaces

If you already have experience with using a command line feel free to skip ahead to the Tools Involved section.

If the technical language is confusing you, there is an easily accessible **Glossary** available in the Courses Side tray. (Swipe right from the edge of the left side of the screen to reveal if you're reading this on a mobile device.)

---

A **"command line interface"** or **C.L.I.** is a technical way of saying you will be typing out in text the instructions you want the computer to perform. This is in contrast to the more common way of using a mouse to click on menus & icons with graphics, images, and buttons.

The text you write are the "commands" you're giving to the computer. The "line" refers to the actual line you are typing on, and the "interface" is the text as it is printed on your screen.

This style of computing is not common for casual computer users, but with a little bit of practice you can learn it.

Each operating system (Windows/OSX/Unix) have a distinct program for interacting with the Command Line. On Windows it is called "Command Prompt" On OSX it is called "Terminal" and Unix has a "sh" or "Shell" and Linux has "Bash". Sometimes you may hear developers refer to any/all of these programs as "shells", and to make it even more complicated it is also used sometimes as generic term that can mean any program at all.

### Learning How to Use a Command Line Interface

If you'd like a good general introduction tutorial, you should check out this ["Introduction to command line"](https://tutorial.djangogirls.org/en/intro_to_command_line/) from the Django Girls.

If you would like something more specific there are a few listed below and also lots of great online tutorials and video walk throughs available as well if you search around.

---

### Additional Links for more in depth Introductions to CLI

* [Introduction to the Mac OS X Command Line](http://blog.teamtreehouse.com/introduction-to-the-mac-os-x-command-line)

* [An Introduction to the Linux Terminal](https://www.digitalocean.com/community/tutorials/an-introduction-to-the-linux-terminal)

* [A Command Line Primer for Beginners](https://lifehacker.com/5633909/who-needs-a-mouse-learn-to-use-the-command-line-for-almost-anything)

---
