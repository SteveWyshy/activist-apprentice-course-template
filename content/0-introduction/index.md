# Introduction

---

An introduction to all the tools and technology you'll need to have setup on your system in order to start creating your Courses.

This section will help you install all the software you need, introduce you to some of it if it's your first time, and also show you how to setup a GitLab account. You'll need to do all of this in order to create and share your Courses.

If the technical language in this section is new to you, there is an Glossary available in the Courses Side tray. (Swipe right from the edge of the left side of the screen to reveal if you're reading this in Expo)

> After you've installed all the Software described in this section, we suggest you restart your computer before you proceed to the next section.

---
