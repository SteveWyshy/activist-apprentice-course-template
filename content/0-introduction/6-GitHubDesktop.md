# GitHub Desktop App

Once you've setup a GitLab account, you'll also want to get the GitHub Desktop app for keeping your work in sync between your computer and your GitLab account.

The free application for managing your software projects for GitHub also works just as well for projects on GitLab. We suggest using it to help keep track of your work and syncing it between your computer and GitLab.

Visit https://desktop.github.com and download the appropriate version for your computer.

Run the installer it provides. For now you don't need to set anything up. Later in the documentation we'll refer to this though and walk you through how to connect it to your GitLab Projects.

**Note:** You do not explicitly need a GitHub account for our purposes.

---
