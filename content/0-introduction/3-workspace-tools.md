# Workspace Tools

To install activist-apprentice we also need to install two other tools now that you have all the other tools installed. We're going to install these tools first and end with activist-apprentice.

* docsmith@beta
* pandoc
* activist-apprentice

---

## docsmith@beta

Go to your Terminal window and run this command: `npm i -g docsmith@beta`

A series of installation commands will run.

You may get this error depending on how the Xcode installation went:

> `xcode-select: note: install requested for command line developer tools`

If you get this error, you can run this command:

`xcode-select --install`

It will download and install some additional tools and also have you confirm/agree to the license agreement. Follow the prompts.

Once it confirms it is installed re-run the initial command `npm i -g docsmith@beta`

---

## pandoc

Go to your Terminal window and run this command: `npm install pandoc`

A series of installation commands will run.

---

## activist-apprentice

Go to your Terminal window and run this command: `npm i -g activist-apprentice`

----

Now we have everything installed! In the next section we'll initialize your first course.

---
