# Atom & Plug-ins

We'll be using a piece of software called Atom to actually write our Courses in. In addition to the default package we'll also install some Atom plug-ins as well. This will walk you through them.

-----

## Installing Atom

Go to this site and download the appropriate version for your computer:

https://atom.io

Once the file is downloaded, open it and install it on your computer.

---

## Installing Atom Plug-ins

We are going to install four plug-ins as well:

* [markdown-preview-enhanced](https://atom.io/packages/markdown-preview-enhanced)
* [linter](https://atom.io/packages/linter)
* [linter-ui-default](https://atom.io/packages/linter-ui-default)
* [linter-remark](https://atom.io/packages/linter-remark)

You can follow these links to learn more about them but we will install them inside of Atom itself to make updating them easier.

With Atom open go to the top menu item labeled **Atom** and look for the menu option **Preferences**

A new window will open that displays the available settings with the **Core** section listed. Look for the section labeled **Install** near the bottom and click on it.

A window will open named **Install Packages** with a search box below it.

Do a search for `markdown-preview-enhanced` and it should come up with a blue install button attached to it. Click the blue install button. The button will animate while the installation happens.

Once the installation finishes three buttons will appear in place of the install button you pressed. The buttons will say "Settings" "Uninstall" and "Disable"

Return to the search box at the top, and search for the additional plug-ins:

* `linter`
* `linter-ui-default`
* `linter-remark`

If Atom asks if you would like to install any dependencies as well, click Yes.

You can now close the Settings panel that opened. To be safe we suggest you exit Atom and re-open it before you begin to make sure everything gets a chance to setup correctly.

---
