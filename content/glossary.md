# Glossary

Let's define some of the language we'll be using throughout this documentation.

---

### People / Roles

## Readers

* The person intended to learn by reading a Course.

## Authors

* The person/people who write/create/adapt content for a Course.

## Publishers

* The person or persons responsible for configuring, publishing, and updating the Course. They need to be comfortable using a command line (or willing to learn!) and familiar with technical tools.

* It's helpful if this is one of the authors, but not required.

## Course Catalog Administrator

* Small World News is responsible for the list of Courses available in [Station](http://www.getstation.org).

* If you have created a Course and would like it to be added to Station's [Course Catalog](https://smallworldnews.gitlab.io/station-repo/fdroid/repo/), please get in touch by creating an  [**Issue**](https://gitlab.com/smallworldnews/station-repo/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=) on the [Available Courses project](https://gitlab.com/smallworldnews/station-repo) [issues board](https://gitlab.com/smallworldnews/station-repo/issues) on GitLab.

---

#### Information Organization

## Course Catalog

* The collection of Courses available in [Station](http://www.getstation.org).


## Course

* An individual App

* A complete collection of information (text and media) intended to educate a reader.

  * We recognize there are a wide variety of words/terms used to describe content that is similar to a Course. (*e.g. Guide, How to, Toolkit, Manual, Review, Module, etc.*) We chose course because it's a short single syllable word that is generally well recognized. We built this tool to be capable of handling all of these different types of content, and encourage you to

## Chapter

* A folder in `/content`.
* Must contain at least one `.md` file, but can contain multiple.
* A significant portion of a Course that covers a specific subject or topic. Typically will include multiple sections internally but can also exist with just one part if needed.
* Chapters should be organized to create a logical flow of information from beginning to the end.
* Chapters are typically numbered individually (e.g. Chapter 1)

## Section

* A `.md` file.
* Length of content is not technically limited, but keeping sections brief for easier referencing should be taken into consideration by Authors.
* A single unit that covers an individual piece of the larger subject or topic of the chapter it's in. Sections are made up of paragraphs of text and sometimes multimedia. A section is the base unit of a Course.
* Authors may choose to further organize them into sub sections. Typically numbered in relation to its corresponding chapter, (e.g. Chapter 1 section 1, or 1.1)

## Sub Section

* An optional way to break up text/media within a Section.
* Typically made up of a sub section title, its corresponding text and maybe some multimedia.
* Authors decide if they are numbered or not. If they are they would be given a third number, e.g. chapter 1, section 1, sub section 1, or 1.1.1)

---

### Technical Terms - Course Specific

## Folder

* Chapters are organized in a Course as folders, the same kind of folders you are familiar with for organizing files locally on your computer. There are also folders named "images" which are used to store image assets.

* Folders currently need to be given names in english text only.

## Page

* An individual markdown file, with the file extension of `.md`.

* Technically speaking these can be as long as the Author chooses, stylistically though Authors should split content up among multiple pages for easier reference.

## `index.md`

* Folders with a `index.md` file become Chapters.

* Folders without `index.md` files are not listed as chapters. Image folders do not get this file.

---

#### Technical Terms - Git specific

## Repo / Project

* What you use to store your Course, the codebase, and the revision history related to it.
* Each individual Course you create will need its own Repo, or **"Project"** as they are referred to on GitLab.
* As an example this documentation exists as its own "Repo", it's posted publicly at:
  * [gitlab.com/contentascode/activist-apprentice-course-template](https://gitlab.com/contentascode/activist-apprentice-course-template)

## CLI (Command Line Interface)

* A Command Line Interface (CLI for short) is a user interface to a computer's operating system or an application in which the user responds to a visual prompt by typing in a command on a specified line, receives a response back from the system, and then enters another command, and so forth. [(definition source)](http://searchwindowsserver.techtarget.com/definition/command-line-interface-CLI)

## Clone

* A clone is a copy of a repo/project that lives on your computer instead of on a website's server somewhere, or the act of making that copy. [(definition source)](https://help.github.com/articles/github-glossary/#clone)

## Fork

* A fork is a personal copy of another user's repo/project that lives on your account. Forks allow you to freely make changes to a project without affecting the original.
* Forks remain attached to the original, allowing you to submit a "pull request" (see below for definition) to the source project with your changes.
* [(definition source)](https://help.github.com/articles/github-glossary/#fork)

## Branch

* A branch is a parallel version of a repo/project. It is contained within the repo, but does not affect the primary or master branch allowing you to work freely without disrupting the "live" version. When you've made the changes you want to make, you can merge your branch back into the master branch to publish your changes. [(definition source)](https://help.github.com/articles/github-glossary/#branch)

## Commit

* A commit, or "revision", is an individual change to a file (or set of files). It's like when you save a file, except with Git, every time you save it creates a unique ID (a.k.a. the "SHA" or "hash") that allows you to keep record of what changes were made when and by who. Commits usually contain a commit message which is a brief description of what changes were made. [(definition source)](https://help.github.com/articles/github-glossary/#commit)

## Push

* Pushing refers to sending your committed changes to a remote repo/project. For instance, if you change something locally, you'd want to then push those changes so that others may access them. [(definition source)](https://help.github.com/articles/github-glossary/#push)

## Pull

* Pull refers to when you are fetching in changes and merging them. For instance, if someone has edited the remote file you're both working on, you'll want to pull in those changes to your local copy so that it's up to date. [(definition source)](https://help.github.com/articles/github-glossary/#pull)

## Pull request (pr)

* Pull requests are proposed changes to a repo/project submitted by a user and accepted or rejected by a repo's collaborators. Like issues, pull requests each have their own discussion forum. For more information, see "About pull requests." [(definition source)](https://help.github.com/articles/github-glossary/#pull-request)

---

To dive deeper into the technical world of Git, check out the official [Git reference page](https://git-scm.com/doc) and [GitHub's glossary](https://help.github.com/articles/github-glossary).

---
