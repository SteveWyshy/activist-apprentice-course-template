# Creating a symlink

Now that we have our GitLab project setup we will want to create a symlink to the locally checked out git repo.

In Terminal

```
cd WORKSPACE/@apprentice/preview
```

Open Atom, or whatever your preferred editor is here.

apprentice load can be done manually by:

`cd ~/.content/packages/activist-apprentice-course-template`

git pull to make sure you have the latest changes (in case someone else made a change)

`apprentice save` can be done manually by:

`cd ~/.content/packages/activist-apprentice-course-template`

`git pull` to make sure you have the latest changes (in case someone else made a change)

`git commit -ma "COMMIT MESSAGE"`

> replace `COMMIT MESSAGE` with a short sentence explaining your changes.

git push to push your changes to gitlab.

---
