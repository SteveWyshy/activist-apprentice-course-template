# Structure

![](images/structure.png)

## Chapters

We use a structure where the chapters are the top level folders, they must contain one index file (`index.md`) and can contain additional "section" .md files.

The idea is that the app's home page will list the top level folders, and clicking on these will displays the list of  "section" level .md files.

---

## Sections

Each chapter can have multiple section level files. For instance, this particular file is named (`0-structure.md`) These section files will contain content that fits on one screen in the app. Authors should understand that one .md file equals one screen.

---

## Top Level Pages

For reference content or meta content (Bibliographies, Glossaries, About pages, Copyright information, etc.) you can create `.md` files and put them in the root content folder. `.md` files that are in this folder are listed in the side tray of the app.

---
