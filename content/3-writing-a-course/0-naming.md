# naming folders and files

Folders and files should all be written in what's called [**kebab-case**](https://en.wikipedia.org/wiki/Letter_case#Special_case_styles ).

* e.g. The-quick-brown-fox-jumps-over-the-lazy-dog

Do not use underscores (*the_quick_brown...*) or spaces (*the quick brown...*) as this may break parts of the overall workflow.

---

In the future we may add support for [camelCase](https://en.wikipedia.org/wiki/Camel_case), but for now to avoid issues please write your folder and file names in **kebab-case**.

---
