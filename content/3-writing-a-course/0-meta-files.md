# Meta Files

Like in published course materials there are often additional pages of information you will want to include in your text.

Some examples of meta-pages you may like to include are:

* Bibliography
* Colophon
* Assessment
* Links
* Appendix
* Index
* Glossary
* Resources
* Colophon
* Footnotes
* Dedication
* Acknowledgements
* Foreword
* Preface
* Introduction
