# Tools for converting Existing Documents

## Google Docs to MarkDown

A lot of material has been stored in Google Docs, while you can download files as .docx files and then run those files through the pandoc files below, you will get a lot of additional markup added that you will need to remove.

You can save yourself the trouble by exporting Markdown straight from Google Docs using the Script tools it supports.

### Adding the Script to your Google Doc

Open up the google doc you would like to export.

Go to Tools > Script Editor, and a new window/tab will open with the name "Untitled Project" with a text box named "code.gs"

Copy and Paste the complete code from this page:

https://github.com/mangini/gdocs2md/blob/master/converttomarkdown.gapps

Go to File > Save, and name the file "ConvertToMarkDown"

### Running the script on your Google Doc:

Now that the script is saved, in the same window go to Run > Fun Function > ConvertToMarkdown

*Depending on where your google doc is hosted you may need to approve this script from being able to access your google docs account. If you work with a shared google account please check with your google account administrator before running this script*


A Converted doc will be mailed to you. Subject will begin "[MARKDOWN_MAKER]" with the google docs filename added afterwards.

The markdown file will be attached in the email.

You can do with it as you please.

---

## .DOCX to Markdown

A tool you've likely

https://pandoc.org/
