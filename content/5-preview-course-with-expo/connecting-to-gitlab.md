# Connecting Your Course on GitLab to Expo.io

You can setup the mobile preview pipeline by going into the gitlab settings for your project, CI/CD, then expand the Secret Variables and add EXP_LOGIN and EXP_PASS with your credentials.

![](images/secret-variables.png)

As long as:
* the app builds correctly
  * You've tested it locally on your machine with `apprentice start`
* the `.gitlab-ci.yml` file is in the repo
* the `preview.yml` file is updated
* the credentials are correct

then the app should be published in expo.

---

If you want to preview locally you’ll need to replace the content of `~/.content/packages/activist-apprentice-course-template` with the content of your repo. The easiest is to use a soft symlink by first removing this folder and then use `ln -s YOUR_FOLDER ~/.content/packages/activist-apprentice-course-template`.

You should now be able to use `apprentice start`

---


.gitlab-ci.yml
.remarkrc
mobile.yml
preview.yml

`package.json`

 * Change the package.json `name` and `description` fields so that they match the projects, for instance for this project is named `activist-apprentice-course-repo` but for the `safely-securely-producing-media` guide it would match the name as it appears in the URL.

`package-lock.json`

* Likewise for this file, change the package.json `name` field so it matches the projects, for instance for this project is named `activist-apprentice-course-repo` but for the `safely-securely-producing-media` guide it would match the name as it appears in the URL.
