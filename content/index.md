# Activist Apprentice Course Template

The Activist Apprentice Course Template is a resource for Authors interested in putting their training materials / course curriculum into an app.

This project is built in partnership with [**Content as Code**](http://iilab.github.io/contentascode/).

---

This repo is both a template to start your own Course projects with and the [documentation](https://gitlab.com/contentascode/activist-apprentice-course-template/tree/master/content) to understand how to make your own Courses. It's written for Authors looking to create App versions of their training materials.

<!-- more -->

To see how the app looks, it is available for preview with [**Expo**](https://expo.io/). Scan the QR code below with your phone using the [Expo app](https://expo.io/tools#client) to explore the contents of it on your device:

![](images/qrcode_AACTexpo.png)

---

## [Content as Code](http://iilab.github.io/contentascode/)

[**iilab**](http://iilab.org/) started [**Content as Code**](https://github.com/iilab/contentascode) in 2016 to develop workflows and technology to improve content re-use and maintainability.

It aims to make content authoring and management benefit from software engineering collaboration best practices. With the stated [design goals](http://iilab.github.io/contentascode/approach/#design-goals) of:

* Focused Writing
* Localisation and translation
* Low barrier to collaboration
* Track changes and manage contributions
* Content Reuse
* Multiple publication channels

---

## Collaboration

[Small World News](https://www.smallworldnews.com) and iilab agreed to collaborate together to add mobile app generation and the Course authoring command line client to Content as Code. Credit goes to [iilab](http://iilab.org/) for developing these particular tools and to iilab and the rest of the open source developers contributing to the wider code base.

For specific details, please see our [License](0-introduction/1-license.md).

----

We recognize this is not going to be for everyone, and there are still a significant number of features that Authors would want before they would consider moving their content into a system like this.

---
